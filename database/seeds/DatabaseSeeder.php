<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(TahunSeeder::class);
        $this->call(ProfilSeeder::class);

        $this->call(TahapSeeder::class);

        $this->call(DanaSeeder::class);
        $this->call(KomponenSeeder::class);

        $this->call(SubKomponenSeeder::class);
        // $this->call(TokoSeeder::class);
        // $this->call(BarangSeeder::class);
        $this->call(MetaSeeder::class);

        // $this->call(NotaSeeder::class);

        // $this->call(NotaBarangSeeder::class);
        // $this->call(BKUSeeder::class);

        $this->call(SaldoSeeder::class);
    }
}
