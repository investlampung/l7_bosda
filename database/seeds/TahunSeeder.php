<?php

use Illuminate\Database\Seeder;

class TahunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Tahun::create(['tahun' => '2019']);
        App\Tahun::create(['tahun' => '2020']);
        App\Tahun::create(['tahun' => '2021']);
        App\Tahun::create(['tahun' => '2022']);
    }
}
