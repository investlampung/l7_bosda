<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'email' => 'admin@smkpelitapesawaran.sch.id',
            'name' => 'Admin',
            'password' => bcrypt('bos1sampai9')
        ]);
        App\User::create([
            'email' => 'azwar@smkpelitapesawaran.sch.id',
            'name' => 'Azwar Amin',
            'password' => bcrypt('bos1sampai9')
        ]);
        App\User::create([
            'email' => 'tohir@smkpelitapesawaran.sch.id',
            'name' => 'Tohir Zain',
            'password' => bcrypt('bos1sampai9')
        ]);
    }
}
