<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBKUSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_k_u_s', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('nota_id')->unsigned();
            $table->foreign('nota_id')->references('id')->on('notas')->onDelete('cascade');
            $table->integer('pajak')->nullable();
            $table->integer('pph')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_k_u_s');
    }
}
