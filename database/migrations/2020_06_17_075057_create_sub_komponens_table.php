<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubKomponensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_komponens', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('komponen_id')->unsigned();
            $table->foreign('komponen_id')->references('id')->on('komponens')->onDelete('cascade');
            $table->string('kode_1');
            $table->string('kode_2')->nullable();
            $table->string('kode_3')->nullable();
            $table->string('sub_komponen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_komponens');
    }
}
