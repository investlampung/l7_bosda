<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDanasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('danas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tahap_id')->unsigned();
            $table->foreign('tahap_id')->references('id')->on('tahaps')->onDelete('cascade');
            $table->date('tanggal');
            $table->date('tanggal_bku');
            $table->integer('dana');
            $table->text('tipe');
            $table->text('ket');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('danas');
    }
}
