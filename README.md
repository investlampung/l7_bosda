## SEMKAPITA BOS

Aplikasi Website untuk membantu dalam perencanaan Dana BOS

* Register, Reset, Validasi Block
* composer dump-autoload
* composer require maatwebsite/excel
* kemudian pada file view tambahkan kode berikut untuk menampilkan format uang IDR
* <td class="center">{{"Rp. ".format_uang($data->total_invoice)}}</td>
* kemudian pada file view tambahkan kode berikut untuk menampilkan format Tanggal Indonesia tanpa menampilkan hari.
* <td class="center">{{tanggal_local($data->date_invoice)}}</td>
* kemudian pada file view tambahkan kode berikut untuk menampilkan format Tanggal Indonesia dengan menampilkan hari.
* <td class="center">{{tanggal_indonesia($data->date_paid)}}</td>
