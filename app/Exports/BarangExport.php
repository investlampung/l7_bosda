<?php

namespace App\Exports;

use App\Barang;
use Maatwebsite\Excel\Concerns\FromCollection;

class BarangExport implements FromCollection
{

    public function collection()
    {
        return Barang::select('barang','satuan','harga')->get();
    }
}
