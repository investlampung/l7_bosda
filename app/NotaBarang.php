<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaBarang extends Model
{
    protected $fillable = [
        'nota_id','barang_id','qty',
    ];

    public function nota()
    {
        return $this->belongsTo('App\Nota', 'nota_id');
    }

    public function barang()
    {
        return $this->belongsTo('App\Barang', 'barang_id');
    }
}
