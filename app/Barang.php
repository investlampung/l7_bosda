<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $fillable = [
        'tahap_id','toko_id','barang','satuan','harga',
    ];

    public function tahap()
    {
        return $this->belongsTo('App\Tahap', 'tahap_id');
    }

    public function toko()
    {
        return $this->belongsTo('App\Toko', 'toko_id');
    }
    
    public function nota_barang()
    {
        return $this->hasMany('App\NotaBarang');
    }
}
