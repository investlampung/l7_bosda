<?php

namespace App\Imports;

use App\Barang;
use App\Tahap;
use Illuminate\Support\Facades\Cookie;
use Maatwebsite\Excel\Concerns\ToModel;

class BarangImport implements ToModel
{

    public function model(array $row)
    {

        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        return new Barang([
            'tahap_id' => $tahap_id->id,
            'toko_id'     => $row[0],
            'barang'     => $row[1],
            'satuan'    => $row[2],
            'harga'    => $row[3],
        ]);
    }
}
