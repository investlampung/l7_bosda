<?php

namespace App\Imports;

use App\Toko;
use Maatwebsite\Excel\Concerns\ToModel;

class TokoImport implements ToModel
{

    public function model(array $row)
    {
        return new Toko([
            'toko'     => $row[0],
            'jenis'    => $row[1]
        ]);
    }
}
