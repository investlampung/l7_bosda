<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BarangRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'barang' => 'required',
            'satuan' => 'required',
            'harga' => 'required'
        ];
    }
    public function messages()
    {
        return [

            'barang.required' => 'Tidak boleh kosong',
            'satuan.required' => 'Tidak boleh kosong',
            'harga.required' => 'Tidak boleh kosong'
        ];
    }
}
