<?php

namespace App\Http\Controllers\Web\Admin;

use App\Dana;
use App\Http\Controllers\Controller;
use App\Komponen;
use App\Meta;
use App\Profil;
use App\Tahap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CetakBPPController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        // $dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Tarik')->orderBy('tanggal_bku', 'ASC')->get()->all();
        // $total_dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Masuk')->sum('dana');

        // $saldo = Saldo::where('tahap_id', $tahap_id->id)->orderBy('tanggal', 'ASC')->get()->all();

        $komponen = Komponen::where('tahap_id', $tahap_id->id)->get()->all();

        return view('admin.cetak.bpp.beranda', compact('komponen', 'no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $no = 1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();
        $komponen = Komponen::where('tahap_id', $tahap_id->id)->get()->all();

        $profil = Profil::all();
        $nama = Profil::where('profil_key','Nama')->get()->first();
        $kab = Profil::where('profil_key','Kabupaten')->get()->first();
        $prov = Profil::where('profil_key','Provinsi')->get()->first();

        $jenis = Meta::where('tahap_id',$tahap_id->id)->where('meta_key','Jenis Kegiatan')->get()->first();
        $pemberi = Meta::where('tahap_id',$tahap_id->id)->where('meta_key','Pemberi Bantuan')->get()->first();
        $bulan = Meta::where('tahap_id',$tahap_id->id)->where('meta_key','Bulan')->get()->first();
        $revisi_rab = Meta::where('tahap_id',$tahap_id->id)->where('meta_key','Revisi RAB')->get()->first();

        $tgl_terakhir = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Tarik')->orderBy('tanggal', 'DESC')->get()->first();
        $tgl_terima = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Masuk')->orderBy('tanggal', 'ASC')->get()->first();
        $total_dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Masuk')->sum('dana');

        return view('admin.cetak.bpp.show', compact('komponen', 'no','profil','nama','kab','prov','jenis','pemberi','bulan','revisi_rab','tgl_terima','tgl_terakhir','total_dana'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
