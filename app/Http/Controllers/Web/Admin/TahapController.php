<?php

namespace App\Http\Controllers\Web\Admin;

use App\Barang;
use App\Dana;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\TahapRequest;
use App\Komponen;
use App\Meta;
use App\Saldo;
use App\Tahap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class TahapController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(TahapRequest $request)
    {
        $id_tahun = Cookie::get('id_tahun');
        $data = $request->all();
        $data['tahun_id'] = $id_tahun;
        Tahap::create($data);

        $tahap_id = Tahap::orderBy('id','DESC')->get()->first();
        $meta1['meta_key'] = "Jenis Kegiatan";
        $meta1['meta_value'] = "";
        $meta1['tahap_id'] = $tahap_id->id;
        Meta::create($meta1);
        $meta2['meta_key'] = "Pemberi Bantuan";
        $meta2['meta_value'] = "";
        $meta2['tahap_id'] = $tahap_id->id;
        Meta::create($meta2);
        $meta3['meta_key'] = "Bulan";
        $meta3['meta_value'] = "";
        $meta3['tahap_id'] = $tahap_id->id;
        Meta::create($meta3);
        $meta4['meta_key'] = "Revisi RAB";
        $meta4['meta_value'] = "";
        $meta4['tahap_id'] = $tahap_id->id;
        Meta::create($meta4);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Tahap " . $request->tahap;
        History::create($histori);

        $notification = array(
            'message' => 'Data Tahap "' . $request->tahap . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('pengaturan.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Tahap $tahap)
    {
        $komponen = Komponen::where('tahap_id', $tahap->id)->get()->all();
        $barang = Barang::where('tahap_id', $tahap->id)->get()->all();
        $dana = Dana::where('tahap_id', $tahap->id)->get()->all();
        $saldo = Saldo::where('tahap_id', $tahap->id)->get()->all();

        if (empty($komponen) && empty($barang) && empty($dana) && empty($saldo)) {
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Hapus";
            $histori['desc_info'] = "Data Tahap " . $tahap->tahap;
            History::create($histori);

            $notification = array(
                'message' => 'Data Tahap "' . $tahap->tahap . '" berhasil dihapus.',
                'alert-type' => 'error'
            );

            $tahap->delete();

            return redirect()->route('pengaturan.index')->with($notification);
        } else {

            $notification = array(
                'message' => 'Data Tahap "' . $tahap->tahap . '" sedang terpakai.',
                'alert-type' => 'warning'
            );

            return redirect()->route('pengaturan.index')->with($notification);
        }
    }
}
