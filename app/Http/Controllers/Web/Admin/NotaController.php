<?php

namespace App\Http\Controllers\Web\Admin;

use App\Barang;
use App\BKU;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\NotaRequest;
use App\Nota;
use App\NotaBarang;
use App\SubKomponen;
use App\Tahap;
use App\Toko;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class NotaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $no = 1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        $nota = Nota::where('sub_komponen_id', $id)
            ->orderBy('id', 'DESC')->get()->all();

        $sub_komponen = SubKomponen::where('id', $id)->get()->first();
        $toko = Toko::orderBy('toko','ASC')->get()->all();
        $sub_komponen_id = $id;

        $barang = Barang::where('tahap_id', $tahap_id->id)->orderBy('barang', 'ASC')->get()->all();

        return view('admin.nota.beranda', compact('nota', 'no', 'sub_komponen_id', 'sub_komponen', 'barang','toko','tahap_id'));
    }

    public function create()
    {
        //
    }

    public function store(NotaRequest $request)
    {
        $data = $request->all();

        $sub_kom = SubKomponen::where('id', $request->sub_komponen_id)->get()->first();

        Nota::create($data);

        $id_nota = Nota::orderBy('tanggal', 'ASC')->get()->first();
        $data_bku['nota_id'] = $id_nota->id;
        BKU::create($data_bku);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Nota " . $sub_kom->sub_komponen;
        History::create($histori);

        $notification = array(
            'message' => 'Data Nota "' . $sub_kom->sub_komponen . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->back()->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $toko = Toko::orderBy('toko','ASC')->get()->all();
        $nota = Nota::findOrFail($id);
        return view('admin.nota.edit', compact('nota','toko'));
    }

    public function update(NotaRequest $request, $id)
    {
        $nota = Nota::findOrFail($id);

        $nota->update($request->all());

        $sub_kom = SubKomponen::where('id', $request->sub_komponen_id)->get()->first();

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Nota " . $sub_kom->sub_komponen;
        History::create($histori);

        $notification = array(
            'message' => 'Data Nota "' . $sub_kom->sub_komponen . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect('/admin/nota/index/' . $request->sub_komponen_id)->with($notification);
    }

    public function destroy($id)
    {
        $nota = Nota::findOrFail($id);

        $sub_kom = SubKomponen::where('id', $nota->sub_komponen_id)->get()->first();

        if (empty($nota->uraian)) {
            $info = $sub_kom->sub_komponen;
        } else {
            $info = $nota->uraian;
        }
        
        $bku = BKU::where('nota_id', $id)->get()->first();
        $notabarang = NotaBarang::where('nota_id', $id)->get()->first();

        if (empty($notabarang)) {
            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Hapus";
            $histori['desc_info'] = "Data Nota " . $info;
            History::create($histori);

            $notification = array(
                'message' => 'Data NOta "' . $info . '" berhasil dihapus.',
                'alert-type' => 'error'
            );

            $nota->delete();
            $bku->delete();

            return redirect('/admin/nota/index/' . $sub_kom->id)->with($notification);
        } else {

            $notification = array(
                'message' => 'Data Nota "' . $info . '" sedang terpakai.',
                'alert-type' => 'warning'
            );

            return redirect('/admin/nota/index/' . $sub_kom->id)->with($notification);
        }
    }
}
