<?php

namespace App\Http\Controllers\Web\Admin;

use App\Exports\TokoExport;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\TokoRequest;
use App\Nota;
use App\Toko;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Imports\TokoImport;
use Excel;

class TokoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $toko = Toko::orderBy('jenis', 'ASC')->orderBy('toko', 'ASC')->get()->all();
        return view('admin.toko.beranda', compact('toko', 'no'));
    }

    public function import(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file'); //GET FILE
            Excel::import(new TokoImport, $file); //IMPORT FILE 
            return redirect()->back();
        }
    }

    public function export() 
    {
        return Excel::download(new TokoExport, 'data_toko.xlsx');
    }

    public function create()
    {
        //
    }

    public function store(TokoRequest $request)
    {
        $data = $request->all();

        Toko::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Toko " . $request->toko;
        History::create($histori);

        $notification = array(
            'message' => 'Data Toko "' . $request->toko . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('toko.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit(Toko $toko)
    {
        return view('admin.toko.edit', compact('toko'));
    }

    public function update(TokoRequest $request, Toko $toko)
    {
        $toko->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Toko " . $request->toko;
        History::create($histori);

        $notification = array(
            'message' => 'Data Toko "' . $request->toko . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('toko.index')->with($notification);
    }

    public function destroy(Toko $toko)
    {
        $nota = Nota::where('toko_id', $toko->id)->get()->first();

        if (empty($nota)) {
            // HISTORI
            $histori['user'] = Auth::user()->name;
            $histori['info'] = "Hapus";
            $histori['desc_info'] = "Data Toko " . $toko->toko;
            History::create($histori);

            $notification = array(
                'message' => 'Data Toko "' . $toko->toko . '" berhasil dihapus.',
                'alert-type' => 'error'
            );

            $toko->delete();

            return redirect()->route('toko.index')->with($notification);
        } else {

            $notification = array(
                'message' => 'Data Toko "' . $toko->toko . '" terinput di Nota.',
                'alert-type' => 'warning'
            );

            return redirect()->route('toko.index')->with($notification);
        }
    }
}
