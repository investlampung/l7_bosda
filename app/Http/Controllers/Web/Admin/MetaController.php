<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\Meta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MetaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $meta = Meta::findOrFail($id);
        return view('admin.meta.edit', compact('meta'));
    }

    public function update(Request $request, $id)
    {
        $meta = Meta::findOrFail($id);
        $meta->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data " . $meta->meta_key . " menjadi ".$meta->meta_value;
        History::create($histori);

        $notification = array(
            'message' => 'Data "' . $meta->meta_key . '" berhasil diubah menjadi '.$meta->meta_value,
            'alert-type' => 'success'
        );

        return redirect()->route('profil.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }
}
