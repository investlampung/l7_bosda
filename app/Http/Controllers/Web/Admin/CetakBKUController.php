<?php

namespace App\Http\Controllers\Web\Admin;

use App\BKU;
use App\Dana;
use App\Http\Controllers\Controller;
use App\Komponen;
use App\Nota;
use App\Profil;
use App\Saldo;
use App\Tahap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CetakBKUController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        $dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Tarik')->orderBy('tanggal_bku', 'ASC')->get()->all();
        $total_dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Masuk')->sum('dana');

        $saldo = Saldo::where('tahap_id', $tahap_id->id)->orderBy('tanggal', 'ASC')->get()->all();

        $komponen = Komponen::where('tahap_id', $tahap_id->id)->get()->all();

        return view('admin.cetak.bku.beranda', compact('komponen', 'no', 'dana', 'saldo', 'total_dana'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $no = 1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        $dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Tarik')->orderBy('tanggal_bku', 'ASC')->get()->all();
        $total_dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Masuk')->sum('dana');

        $saldo = Saldo::where('tahap_id', $tahap_id->id)->orderBy('tanggal', 'ASC')->get()->all();
        $profil = Profil::all();
        $komponen = Komponen::where('tahap_id', $tahap_id->id)->get()->all();

        $tgl_dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Tarik')->orderBy('tanggal', 'DESC')->get()->first();

        return view('admin.cetak.bku.show', compact('komponen', 'no', 'dana', 'saldo', 'total_dana','profil','tgl_dana'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
