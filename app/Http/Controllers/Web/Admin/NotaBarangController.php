<?php

namespace App\Http\Controllers\Web\Admin;

use App\History;
use App\Http\Controllers\Controller;
use App\NotaBarang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotaBarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $data = $request->all();

        NotaBarang::create($data);

        $barang = NotaBarang::orderBy('id','DESC')->get()->first();

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Barang " . $barang->barang->barang;
        History::create($histori);

        $notification = array(
            'message' => 'Data Barang "' . $barang->barang->barang . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->back()->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $data = NotaBarang::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Barang " . $data->barang->barang;
        History::create($histori);

        $notification = array(
            'message' => 'Data Barang "' . $data->barang->barang . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->back()->with($notification);
    }
}
