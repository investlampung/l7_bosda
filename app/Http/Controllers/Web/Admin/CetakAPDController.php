<?php

namespace App\Http\Controllers\Web\Admin;

use App\Dana;
use App\Http\Controllers\Controller;
use App\Komponen;
use App\Tahap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CetakAPDController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        $dana = Dana::where('tahap_id', $tahap_id->id)->orderBy('tanggal', 'ASC')->get()->all();
        
        $komponen = Komponen::where('tahap_id', $tahap_id->id)->orderBy('kode_komponen', 'ASC')->get()->all();
        $total_dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe','Masuk')->sum('dana');
        return view('admin.cetak.apd.beranda', compact('komponen', 'no','dana','total_dana'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $no = 1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        $dana = Dana::where('tahap_id', $tahap_id->id)->orderBy('tanggal', 'ASC')->get()->all();
        
        $komponen = Komponen::where('tahap_id', $tahap_id->id)->orderBy('kode_komponen', 'ASC')->get()->all();
        $total_dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe','Masuk')->sum('dana');
        return view('admin.cetak.apd.show', compact('komponen', 'no','dana','total_dana'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
