<?php

namespace App\Http\Controllers\Web\Admin;

use App\Dana;
use App\Http\Controllers\Controller;
use App\Komponen;
use App\Profil;
use App\Tahap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CetakBKPController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no = 1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        // $dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Tarik')->orderBy('tanggal_bku', 'ASC')->get()->all();
        // $total_dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Masuk')->sum('dana');

        // $saldo = Saldo::where('tahap_id', $tahap_id->id)->orderBy('tanggal', 'ASC')->sum('saldo');
        $profil = Profil::all();
        $komponen = Komponen::where('tahap_id', $tahap_id->id)->get()->all();
        $tgl_dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Tarik')->orderBy('tanggal', 'DESC')->get()->first();

        return view('admin.cetak.bkp.beranda', compact('komponen', 'no','profil','tgl_dana'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $no = 1;
        $tahap = Cookie::get('tahap');
        $tahun_id = Cookie::get('id_tahun');
        $tahap_id = Tahap::where('tahun_id', $tahun_id)->where('tahap', $tahap)->get()->first();

        // $dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Tarik')->orderBy('tanggal_bku', 'ASC')->get()->all();
        // $total_dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Masuk')->sum('dana');

        // $saldo = Saldo::where('tahap_id', $tahap_id->id)->orderBy('tanggal', 'ASC')->sum('saldo');
        $profil = Profil::all();
        $komponen = Komponen::where('tahap_id', $tahap_id->id)->get()->all();
        $tgl_dana = Dana::where('tahap_id', $tahap_id->id)->where('tipe', 'Tarik')->orderBy('tanggal', 'DESC')->get()->first();

        return view('admin.cetak.bkp.show', compact('komponen', 'no','profil','tgl_dana'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
