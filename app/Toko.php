<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{
    protected $guarded = [];

    protected $fillable = [
        'toko', 'jenis', 'pemilik'
    ];

    public function nota()
    {
        return $this->hasMany('App\Nota');
    }
    public function barang()
    {
        return $this->hasMany('App\Barang');
    }
}
