<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahun extends Model
{
    protected $fillable = [
        'tahun',
    ];

    //tahap
    public function tahap()
    {
        return $this->hasMany('App\Tahap');
    }
}
