<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::get('/home', 'HomeController@index');
Route::resource('/admin/beranda', 'Web\Admin\BerandaController');
Route::resource('/admin/profil', 'Web\Admin\ProfilController');
Route::resource('/admin/meta', 'Web\Admin\MetaController');
Route::resource('/admin/history', 'Web\Admin\HistoryController');
Route::resource('/admin/pengaturan', 'Web\Admin\PengaturanController');
Route::resource('/admin/tahun', 'Web\Admin\TahunController');
Route::resource('/admin/tahap', 'Web\Admin\TahapController');
Route::resource('/admin/rubah', 'Web\Admin\CookieController');

Route::resource('/admin/dana', 'Web\Admin\DanaController');
Route::resource('/admin/saldo', 'Web\Admin\SaldoController');

Route::resource('/admin/komponen', 'Web\Admin\KomponenController');
Route::get('/admin/subkomponen/index/{id}', 'Web\Admin\SubKomponenController@index');
Route::resource('/admin/subkomponen', 'Web\Admin\SubKomponenController');

Route::get('/admin/nota/index/{id}', 'Web\Admin\NotaController@index');
Route::resource('/admin/nota', 'Web\Admin\NotaController');

Route::resource('/admin/barang', 'Web\Admin\BarangController');
Route::post('importbarang', 'Web\Admin\BarangController@import')->name('importbarang');
Route::get('exportbarang', 'Web\Admin\BarangController@export')->name('exportbarang');

Route::resource('/admin/toko', 'Web\Admin\TokoController');
Route::post('importtoko', 'Web\Admin\TokoController@import')->name('importtoko');
Route::get('exporttoko', 'Web\Admin\TokoController@export')->name('exporttoko');

Route::resource('/admin/notabarang', 'Web\Admin\NotaBarangController');

Route::resource('/admin/cetak/notas', 'Web\Admin\CetakNotaController');
Route::resource('/admin/cetak/apd', 'Web\Admin\CetakAPDController');
Route::resource('/admin/cetak/bku', 'Web\Admin\CetakBKUController');
Route::resource('/admin/cetak/bpp', 'Web\Admin\CetakBPPController');
Route::resource('/admin/cetak/rlpd', 'Web\Admin\CetakRLPDController');
Route::resource('/admin/cetak/bkp', 'Web\Admin\CetakBKPController');