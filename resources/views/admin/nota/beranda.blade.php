@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Nota
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <a href="{{url('admin/komponen')}}" class="btn btn-primary">Komponen</a>
      <a href="{{url('admin/subkomponen/index')}}/{{$sub_komponen->komponen->id}}" class="btn btn-success">Sub Komponen</a>
      <br><br>
    </div>
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Nota - {{$sub_komponen->sub_komponen}}</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <form action="{{ route('nota.store') }}" class="form-horizontal" method="POST">
            @csrf

            <div class="col-md-12">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="toko_id" class="col-sm-4 control-label">Toko</label>

                  <div class="col-sm-8">
                    <select class="form-control" name="toko_id">
                      @foreach($toko as $tok)
                      <option value="{{$tok->id}}">{{$tok->toko}}</option>
                      @endforeach
                    </select>
                    <small class="text-danger">{{ $errors->first('kode_pajak') }}</small>
                  </div>
                </div>

                <div class="form-group">
                  <label for="uraian" class="col-sm-4 control-label">Uraian</label>

                  <div class="col-sm-8">
                    <input type="text" name="uraian" class="form-control" placeholder="Uraian (Pengganti Nama Sub Komponen)">
                    <small class="text-danger">{{ $errors->first('uraian') }}</small>
                  </div>
                </div>
                <div class="form-group">
                  <label for="kode_rek" class="col-sm-4 control-label">Kode Rekening</label>

                  <div class="col-sm-8">
                    <input type="text" name="kode_rek" class="form-control" placeholder="ex : 5-2-2-01-01">
                    <small class="text-danger">{{ $errors->first('kode_rek') }}</small>
                  </div>
                </div>

                <div class="form-group">
                  <label for="tanggal" class="col-sm-4 control-label">Tanggal</label>

                  <div class="col-sm-8">
                    <input type="date" name="tanggal" class="form-control" placeholder="Tanggal">
                    <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="tanggal_bku" class="col-sm-4 control-label">Tanggal BKU</label>

                  <div class="col-sm-8">
                    <input type="date" name="tanggal_bku" class="form-control" placeholder="Tanggal BKU">
                    <small class="text-danger">{{ $errors->first('tanggal_bku') }}</small>
                  </div>
                </div>
                <div class="form-group">
                  <label for="kode_pajak" class="col-sm-4 control-label">Pajak</label>

                  <div class="col-sm-8">
                    <select class="form-control" name="kode_pajak">
                      <option value="T">Ada Pajak</option>
                      <option value="F">Tidak Ada Pajak</option>
                    </select>
                    <small class="text-danger">{{ $errors->first('kode_pajak') }}</small>
                  </div>
                </div>

                <div class="form-group">
                  <label for="kode_pph" class="col-sm-4 control-label">PPh</label>

                  <div class="col-sm-8">
                    <select class="form-control" name="kode_pph">
                      <option value="21">PPh 21</option>
                      <option value="22">PPh 22</option>
                      <option value="-">Tidak Ada PPh</option>
                    </select>
                    <small class="text-danger">{{ $errors->first('kode_pph') }}</small>
                  </div>
                </div>
              </div>
              <input type="hidden" name="sub_komponen_id" class="form-control" value="{{$sub_komponen_id}}">
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div>

          </form>
        </div>
      </div>

      @foreach($nota as $item)
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">
            @if($item->uraian=="")
            {{$sub_komponen->sub_komponen}}
            @else
            {{$item->uraian}}
            @endif
          </h3>
        </div>

        <div class="box-body">
          <span class="label label-warning" style="margin-right:5px;">Toko : {{ $item->toko->toko}}</span>
          <span class="label label-danger" style="margin-right:5px;">
            @if($item->kode_pajak=="T")
            Ada Pajak
            @else
            Tidak Ada Pajak
            @endif
          </span>
          @if($item->kode_pph=="22")
          <span class="label label-success" style="margin-right:5px;">PPh 22</span>
          @elseif($item->kode_pph=="21")
          <span class="label label-success" style="margin-right:5px;">PPh21</span>
          @else
          @endif
          <span class="label label-primary" style="margin-right:5px;">{{$item->kode_rek}}</span>
          <span class="label label-info" style="margin-right:5px;">Tgl Nota : {{ $item->tanggal->format('d-m-Y') }}</span>
          <span class="label label-warning" style="margin-right:5px;">
          @if(empty($item->tanggal_bku))
            Tgl BKU : 
          @else
            Tgl BKU : {{ $item->tanggal_bku->format('d-m-Y') }}
          @endif
          </span>
          <hr>

          <table class="table table-bordered">
            <tbody>
              <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Nama Barang</th>
                <th colspan="2">Banyak</th>
                <th rowspan="2">Harga</th>
                <th rowspan="2">Jumlah</th>
              </tr>
              <tr>
                <th>Qty</th>
                <th>Satuan</th>
              </tr>

              @php $no=1; @endphp
              @php $jum_harga =0; @endphp

              @foreach($item->nota_barang as $data)

              <tr>
                <td>{{$no++}}</td>
                <td>{{$data->barang->barang}}</td>
                <td>{{$data->qty}}</td>
                <td align="right">{{$data->barang->satuan}}</td>
                <td align="right">{{ number_format($data->barang->harga, 0, ".", ".")}}</td>
                <td align="right">
                  {{number_format($data->barang->harga*$data->qty, 0, ".", ".") }}
                </td>
                @php
                $jum_harga = $jum_harga + ($data->barang->harga*$data->qty);
                @endphp
              </tr>
              @endforeach
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right" style="background-color:#ffbc00">
                  {{ number_format($jum_harga, 0, ".", ".") }}
                </td>
              </tr>
              @php
              $r_ppn=0;
              $r_pph21=0;
              $r_pph22=0;
              $t_pajak=0;
              $t_harga=0;
              @endphp
              @if($item->kode_pajak=="T")
              @if($item->kode_pph=="22")
              @if($jum_harga>=1000000)
              @php
              $dasar_pajak = (100/110)*$jum_harga;
              $ppn = (10/100)*$dasar_pajak;
              $r_ppn = round($ppn, -2);
              $pph22 = (1.5/100)*$dasar_pajak;
              $r_pph22 = round($pph22, -2);
              $t_pajak = $r_ppn+$r_pph22;
              $t_harga = $jum_harga+$t_pajak;
              @endphp
              @endif
              <tr>
                <td></td>
                <td align="right">PPn</td>
                <td></td>
                <td></td>
                <td align="right" style="background-color:#8cde92">{{ number_format($r_ppn, 0, ".", ".") }}</td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td align="right">PPh.22</td>
                <td></td>
                <td></td>
                <td align="right" style="background-color:#8cde92">{{ number_format($r_pph22, 0, ".", ".") }}</td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>Jumlah Pajak</td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right" style="background-color:#8cde92">{{ number_format($t_pajak, 0, ".", ".") }}</td>
              </tr>
              <tr>
                <td></td>
                <td><b>Total</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right" style="background-color:#f1885f"><b>{{ number_format($t_harga, 0, ".", ".") }}</b></td>
              </tr>
              @elseif($item->kode_pph=="21")
              @if($jum_harga>=1000000)
              @php
              $dasar_pajak = (100/110)*$jum_harga;
              $pph21 = (5/100)*$dasar_pajak;
              $r_pph21 = round($pph21, -2);
              $t_harga = $jum_harga+$r_pph21;
              @endphp
              @endif
              <tr>
                <td></td>
                <td align="right">PPh.21</td>
                <td></td>
                <td></td>
                <td align="right" style="background-color:#8cde92">{{ number_format($r_pph21, 0, ".", ".") }}</td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>Jumlah Pajak</td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right" style="background-color:#8cde92">{{ number_format($r_pph21, 0, ".", ".") }}</td>
              </tr>
              <tr>
                <td></td>
                <td><b>Total</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right" style="background-color:#f1885f"><b>{{ number_format($t_harga, 0, ".", ".") }}</b></td>
              </tr>
              @else
              @endif
              @else


              <tr>
                <td></td>
                <td><b>Total</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right" style="background-color:#f1885f"><b>{{ number_format($jum_harga, 0, ".", ".") }}</b></td>
              </tr>
              @endif
            </tbody>
          </table>
        </div>
        <div class="box-footer clearfix" align="right">
          <form action="{{ route('nota.destroy',$item->id) }}" method="POST">

            <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#modal-barang{{$item->id }}">Barang</a>
            <a class="btn btn-success" href="{{ route('nota.edit',$item->id) }}">Ubah</a>
            @csrf
            @method('DELETE')

            <button type="submit" class="btn btn-danger">Hapus</button>
          </form>
        </div>
        <!-- Barang -->
        <div class="modal fade in" id="modal-barang{{$item->id }}">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">
                  Data Barang -
                  @if($item->uraian=="")
                  {{$sub_komponen->sub_komponen}}
                  @else
                  {{$item->uraian}}
                  @endif
                </h4>
              </div>

              <div class="box-body">
                <form action="{{ route('notabarang.store') }}" class="form-horizontal" method="POST">
                  @csrf

                  <div class="form-group">
                    <label for="tahun" class="col-sm-3 control-label">Barang</label>

                    <div class="col-sm-9">
                      <select class="form-control" name="barang_id">
                        @foreach($item->toko->barang as $data)
                        @if($data->tahap_id === $tahap_id->id)
                        <option value="{{$data->id}}">{{$data->barang}} | {{ number_format($data->harga, 0, ".", ".") }}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="qty" class="col-sm-3 control-label">Qty</label>

                    <div class="col-sm-9">
                      <input type="number" name="qty" class="form-control" placeholder="Qty">
                      <small class="text-danger">{{ $errors->first('qty') }}</small>
                    </div>
                  </div>
                  <input type="hidden" name="nota_id" class="form-control" value="{{$item->id}}">
                  <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                </form>
              </div>
              <hr>
              <div class="box-body">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <th>No</th>
                      <th>Nama Barang</th>
                      <th>Qty</th>
                      <th>Hapus</th>
                    </tr>
                    @php
                    $no=1;
                    @endphp
                    @foreach($item->nota_barang as $data)
                    <tr>
                      <td>{{$no++}}</td>
                      <td>{{$data->barang->barang}}</td>
                      <td>{{$data->qty}}</td>
                      <td>
                        <form action="{{ route('notabarang.destroy',$data->id) }}" method="POST">
                          @csrf
                          @method('DELETE')

                          <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                        </form>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <div class="box-footer">
                <button class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endforeach

    </div>
  </div>
</div>
@endsection