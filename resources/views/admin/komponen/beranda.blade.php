@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Alokasi Pengelolaan Dana
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Komponen</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <form action="{{ route('komponen.store') }}" class="form-horizontal" method="POST">
            @csrf

            <div class="col-md-6">
              <div class="form-group">
                <label for="kode" class="col-sm-2 control-label">Kode</label>

                <div class="col-sm-10">
                  <input type="text" name="kode_komponen" class="form-control" placeholder="Kode">
                  <small class="text-danger">{{ $errors->first('kode_komponen') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="komponen" class="col-sm-2 control-label">komponen</label>

                <div class="col-sm-10">
                  <input type="text" name="komponen" class="form-control" placeholder="Komponen">
                  <small class="text-danger">{{ $errors->first('komponen') }}</small>
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div>

          </form>
        </div>
        <hr>
        <div class="box-body" style="overflow-x:auto;">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode</th>
                <th>Komponen</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              @foreach($komponen as $item)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $item->kode_komponen }}</td>
                <td>{{ $item->komponen }}</td>
                <td align="center">
                  <form action="{{ route('komponen.destroy',$item->id) }}" method="POST">
                    <a class="btn btn-primary" href="{{ url('/admin/subkomponen/index',$item->id) }}">Sub Komponen</a>
                    <a class="btn btn-success" href="{{ route('komponen.edit',$item->id) }}">Ubah</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Hapus</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection