@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Komponen
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah Data Komponen</h3>
        </div>

        <div class="box-body">
          <form action="{{ route('komponen.update', $komponen->id) }}" class="form-horizontal" method="POST">
            @csrf
            @method('PUT')

            <div class="form-group">
              <label for="kode" class="col-sm-2 control-label">Kode</label>

              <div class="col-sm-10">
                <input type="text" name="kode_komponen" value="{{$komponen->kode_komponen}}" class="form-control" placeholder="Kode">
                <small class="text-danger">{{ $errors->first('kode_komponen') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="komponen" class="col-sm-2 control-label">Komponen</label>

              <div class="col-sm-10">
                <input type="text" name="komponen" value="{{$komponen->komponen}}" class="form-control" placeholder="Komponen">
                <small class="text-danger">{{ $errors->first('komponen') }}</small>
              </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right">Simpan</button>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection