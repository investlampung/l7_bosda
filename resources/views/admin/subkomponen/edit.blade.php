@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Komponen
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah Data Komponen</h3>
        </div>

        <div class="box-body">
          <form action="{{ route('subkomponen.update', $subkomponen->id) }}" class="form-horizontal" method="POST">
            @csrf
            @method('PUT')

            <div class="form-group">
              <label for="kode_1" class="col-sm-4 control-label">Kode 1</label>

              <div class="col-sm-8">
                <input type="text" value="{{$subkomponen->kode_1}}" name="kode_1" class="form-control" placeholder="Kode 1">
                <small class="text-danger">{{ $errors->first('kode_1') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="kode_2" class="col-sm-4 control-label">Kode 2</label>

              <div class="col-sm-8">
                <input type="text" value="{{$subkomponen->kode_2}}" name="kode_2" class="form-control" placeholder="Kode 2">
                <small class="text-danger">{{ $errors->first('kode_2') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="kode_3" class="col-sm-4 control-label">Kode 3</label>

              <div class="col-sm-8">
                <input type="text" name="kode_3" value="{{$subkomponen->kode_3}}" class="form-control" placeholder="Kode 3">
                <small class="text-danger">{{ $errors->first('kode_3') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="komponen" class="col-sm-4 control-label">Sub Komponen</label>

              <div class="col-sm-8">
                <input type="text" name="sub_komponen" value="{{$subkomponen->sub_komponen}}" class="form-control" placeholder="Sub Komponen">
                <small class="text-danger">{{ $errors->first('sub_komponen') }}</small>
              </div>
            </div>
            <input type="hidden" name="komponen_id" class="form-control" value="{{$subkomponen->komponen_id}}">

            <a href="{{url('admin/subkomponen/index')}}/{{$subkomponen->komponen_id}}" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </form>
        </div>

      </div>
    </div>
  </div>
</div>
</div>
@endsection