@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Toko
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <a href="{{ route('exporttoko') }}" class="btn btn-primary">Download Data Toko</a><br><br>
    </div>
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Toko</h3>
        </div>
        <div class="box-body" style="overflow-x:auto;">
          <div class="col-md-6">
            <form action="{{ route('toko.store') }}" class="form-horizontal" method="POST">
              @csrf
              <div class="form-group">
                <label for="toko" class="col-sm-4 control-label">Nama Toko</label>

                <div class="col-sm-8">
                  <input type="text" name="toko" class="form-control" placeholder="Nama Toko">
                  <small class="text-danger">{{ $errors->first('toko') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="jenis" class="col-sm-4 control-label">Jenis Toko</label>

                <div class="col-sm-8">
                  <input type="text" name="jenis" class="form-control" placeholder="ex : Fotocopy / Komputer / Olahraga">
                  <small class="text-danger">{{ $errors->first('jenis') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="pemilik" class="col-sm-4 control-label">Pemilik</label>

                <div class="col-sm-8">
                  <input type="text" name="pemilik" class="form-control" placeholder="Pemilik">
                  <small class="text-danger">{{ $errors->first('pemilik') }}</small>
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>

            </form>
          </div>
          <div class="col-md-6">
            <form action="{{ route('importtoko') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="toko" class="col-sm-4 control-label">Import Toko</label>
                <div class="col-sm-8">
                  <input type="file" name="file" class="form-control">
                  Pastikan file ber extensi .xls atau .xlsx
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Unggah</button>
            </form>
          </div>
        </div>
        <hr>
        <div class="box-body" style="overflow-x:auto;">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Jenis Toko</th>
                <th>Toko</th>
                <th>Pemilik</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              @foreach($toko as $item)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $item->jenis }}</td>
                <td>{{ $item->toko }}</td>
                <td>{{ $item->pemilik }}</td>
                <td align="center">
                  <form action="{{ route('toko.destroy',$item->id) }}" method="POST">
                    <a class="btn btn-success" href="{{ route('toko.edit',$item->id) }}">Ubah</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Hapus</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection