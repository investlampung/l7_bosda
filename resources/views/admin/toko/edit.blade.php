@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Toko
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah Data Toko</h3>
        </div>

        <div class="box-body">
          <form action="{{ route('toko.update', $toko->id) }}" class="form-horizontal" method="POST">
            @csrf
            @method('PUT')

            <div class="form-group">
              <label for="toko" class="col-sm-2 control-label">Nama Toko</label>

              <div class="col-sm-10">
                <input type="text" name="toko" value="{{$toko->toko}}" class="form-control" placeholder="Nama Toko">
                <small class="text-danger">{{ $errors->first('toko') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="jenis" class="col-sm-2 control-label">Jenis Toko</label>

              <div class="col-sm-10">
                <input type="text" name="jenis" value="{{$toko->jenis}}" class="form-control" placeholder="ex : Fotocopy / Komputer / Olahraga">
                <small class="text-danger">{{ $errors->first('jenis') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="pemilik" class="col-sm-2 control-label">Pemilik</label>

              <div class="col-sm-10">
                <input type="text" value="{{$toko->pemilik}}" name="pemilik" class="form-control" placeholder="Pemilik">
                <small class="text-danger">{{ $errors->first('pemilik') }}</small>
              </div>
            </div>

            <a href="{{url('admin/toko')}}" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </form>
        </div>

      </div>
    </div>
  </div>
</div>
</div>
@endsection