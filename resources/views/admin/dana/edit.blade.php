@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Dana
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah Data Dana</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <form action="{{ route('dana.update', $dana->id) }}" class="form-horizontal" method="POST">
            @csrf
            @method('PUT')

            <div class="col-md-12">
              <div class="form-group">
                <label for="tanggal" class="col-sm-2 control-label">Tanggal</label>

                <div class="col-sm-10">
                  <input type="date" name="tanggal" class="form-control" value="{{ $dana->tanggal->format('Y-m-d') }}">
                  <small class="text-danger">{{ $errors->first('tanggal') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="tanggal_bku" class="col-sm-2 control-label">Tanggal BKU</label>

                <div class="col-sm-10">
                  <input type="date" name="tanggal_bku" value="{{ $dana->tanggal_bku->format('Y-m-d') }}" class="form-control" placeholder="Tanggal BKU">
                  <small class="text-danger">{{ $errors->first('tanggal_bku') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="tipe" class="col-sm-2 control-label">Tipe</label>
                <div class="col-sm-10">
                  <select class="form-control" name="tipe">
                    <option value="{{$dana->tipe}}">{{$dana->tipe}}</option>
                    <option value="Tarik">Tarik</option>
                    <option value="Masuk">Masuk</option>
                  </select>
                  <small class="text-danger">{{ $errors->first('tipe') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="tahun" class="col-sm-2 control-label">Dana</label>

                <div class="col-sm-10">
                  <input type="number" name="dana" value="{{ $dana->dana }}" class="form-control" placeholder="Dana">
                  <small class="text-danger">{{ $errors->first('dana') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="ket" class="col-sm-2 control-label">Keterangan</label>

                <div class="col-sm-10">
                  <input type="text" name="ket" value="{{ $dana->ket }}" class="form-control" placeholder="ex : Penarikan Dana 1">
                  <small class="text-danger">{{ $errors->first('ket') }}</small>
                </div>
              </div>
              <a href="{{url('admin/dana')}}" class="btn btn-default">Kembali</a>
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection