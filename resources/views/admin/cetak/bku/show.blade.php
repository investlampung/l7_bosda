<html>

</html>
<!DOCTYPE html>
<html>

<head>
  <title>Buku Kas Umum</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
  <style>
    @media print {
      @page {
        size: auto !important
      }
    }

    body {
      font-family: 'Source Sans Pro', sans-serif;
    }

    #tabless {
      border: 0px solid black;
    }

    .tablee,
    td,
    th {
      border: 1px solid black;
    }

    .tablee {
      border-collapse: collapse;
      width: 100%;
      margin-bottom: 10px;
    }

    th {
      padding: 5px 5px;
      height: 20px;
      text-align: center;
      font-weight: bold;
    }

    td {
      padding-left: 5px;
      padding-right: 5px;
    }
  </style>
</head>

<body">
  <!-- onload="window.print()" -->
  <center>
    <h4 class="box-title">BUKU KAS UMUM<br>Periode {{Cookie::get('tahap')}} Tahun {{Cookie::get('tahun')}}</h4>
  </center>
  <br>
  @foreach($profil as $sekolah)
  @if($sekolah->profil_key==="Nama")
  <div class="row">
    <div class="col-2">Nama Sekolah</div>
    <div class="col-3">: {{$sekolah->profil_value}}</div>
  </div>
  @elseif($sekolah->profil_key==="Desa")
  <div class="row">
    <div class="col-2">Desa</div>
    <div class="col-3">: {{$sekolah->profil_value}}</div>
  </div>
  @elseif($sekolah->profil_key==="Kabupaten")
  <div class="row">
    <div class="col-2">Kabupaten</div>
    <div class="col-3">: {{$sekolah->profil_value}}</div>
  </div>
  @elseif($sekolah->profil_key==="Provinsi")
  <div class="row">
    <div class="col-2">Provinsi</div>
    <div class="col-3">: {{$sekolah->profil_value}}</div>
  </div>
  @else
  @endif
  @endforeach
  <br>
  <table class="tablee">
    <thead>
      <tr>
        <th rowspan="2" width="35px" align="center">No</th>
        <th rowspan="2" align="center" width="100px">Tanggal</th>
        <th colspan="2" align="center">Transaksi</th>
        <th rowspan="2" align="center">Kode Rekening</th>
        <th rowspan="2" align="center">Uraian</th>
        <th colspan="2" align="center">Mutasi</th>
        <th rowspan="2" align="center">Saldo</th>
        <th rowspan="2" align="center">Ket</th>
      </tr>
      <tr>
        <th align="center">Bukti</th>
        <th align="center" width="100px">Tanggal</th>
        <th align="center">Debet</th>
        <th align="center">Kridit</th>
      </tr>
    </thead>
    <tbody>
      @php
      $total_saldo=0;
      $total_debet=0;
      @endphp
      <!-- SALDO -->
      @foreach($saldo as $sald)
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>{{$sald->ket}}</td>
        <td></td>
        <td></td>
        <td align="right">{{ number_format($sald->saldo, 0, ".", ".")}}</td>
        @php
        $total_saldo = $total_saldo+$sald->saldo;
        @endphp
        <td></td>
      </tr>
      @endforeach
      <!-- END SALDO -->
      <tr height="20px">
        <td colspan="10"></td>
      </tr>
      <!-- DANA -->

      @php
      $tanggal_urut;
      $tanggal_temp;
      $no_kode="";
      @endphp

      @foreach($komponen as $item)
      @foreach($item->sub_komponen as $subkom)

      @foreach($subkom->nota->sortBy('tanggal') as $nota)
      @php
      $tanggal_urut = $nota->tanggal;
      @endphp

      @php $total = 0; @endphp

      @foreach($dana as $data)

      @if(empty($no_kode))
      @php
      $tanggal_temp = $data->tanggal_bku;
      $no_kode++;
      @endphp
      @endif

      @if($tanggal_temp < $data->tanggal_bku)
        @php
        $tanggal_temp=$data->tanggal_bku;
        @endphp
        @endif
        @if($tanggal_urut >= $data->tanggal_bku and $data->tanggal_bku == $tanggal_temp)
        @if($data->tipe==="Tarik")
        <tr style="background-color:#ff9898">
          <td></td>
          <td align="center">{{ $data->tanggal->format('d-m-Y') }}</td>
          <td></td>
          <td></td>
          <td></td>
          <td>{{$data->ket}}</td>
          <td align="right">{{ number_format($data->dana, 0, ".", ".")}}</td>
          <td></td>
          <td align="right">
            @php
            $total_saldo = $total_saldo+$data->dana;
            @endphp
            {{ number_format($total_saldo, 0, ".", ".")}}
          </td>
          <td></td>
        </tr>
        @endif
        @endif
        @endforeach
        <tr>
          @php $m = $no; @endphp
          <td align="center">{{ $no++ }}</td>
          <td align="center">
            @if(empty($nota->tanggal_bku))
            @else
            {{ $nota->tanggal_bku->format('d-m-Y') }}
            @endif
          </td>
          <td align="center">{{$m}}</td>
          <td align="center">{{ $nota->tanggal->format('d-m-Y') }}</td>
          <td align="center">{{$nota->kode_rek}}</td>
          <td>
            @if($nota->uraian=="")
            Pelunasan {{$nota->sub_komponen->sub_komponen}}
            @else
            Pelunasan {{$nota->uraian}}
            @endif
          </td>
          <td>{{$nota->toko->toko}}</td>
          <td align="right">
            @php $jum_harga = 0; @endphp
            <!-- NOTA BARANG -->
            @foreach($nota->nota_barang as $hem)
            @php
            $jum_harga = $jum_harga + ($hem->barang->harga*$hem->qty);
            @endphp
            @endforeach
            <!-- END NOTA BARANG -->
            {{ number_format($jum_harga, 0, ".", ".")}}
          </td>
          <td align="right">
            @php
            $total_saldo = $total_saldo-$jum_harga;
            @endphp
            {{ number_format($total_saldo, 0, ".", ".")}}
          </td>
          <td></td>
        </tr>
        @php
        $total_debet=$total_debet+$jum_harga;
        @endphp
        <!-- IF KODE PAJAK -->
        @if($nota->kode_pajak=="T")
        <!-- IF KODE PPH 22-->
        @if($nota->kode_pph=="22")
        @if($jum_harga>=1000000)
        @php
        $dasar_pajak = (100/110)*$jum_harga;
        $ppn = (10/100)*$dasar_pajak;
        $r_ppn = round($ppn, -2);
        $pph22 = (1.5/100)*$dasar_pajak;
        $r_pph22 = round($pph22, -2);
        $total_debet=$total_debet+$r_pph22+$r_ppn;
        @endphp
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right">PPn</td>
          <td></td>
          <td align="right">{{ number_format($r_ppn, 0, ".", ".")}}</td>
          <td align="right">
            @php
            $total_saldo = $total_saldo-$r_ppn;
            @endphp
            {{ number_format($total_saldo, 0, ".", ".")}}
          </td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right">PPh.22</td>
          <td></td>
          <td align="right">{{ number_format($r_pph22, 0, ".", ".")}}</td>
          <td align="right">
            @php
            $total_saldo = $total_saldo-$r_pph22;
            @endphp
            {{ number_format($total_saldo, 0, ".", ".")}}
          </td>
          <td></td>
        </tr>
        @endif
        <!-- END KODE PPH 22 -->
        <!-- IF KODE PPH 21 -->
        @elseif($nota->kode_pph=="21")
        @if($jum_harga>=1000000)
        @php
        $dasar_pajak = (100/110)*$jum_harga;
        $pph21 = (5/100)*$dasar_pajak;
        $r_pph21 = round($pph21, -2);
        $total_debet=$total_debet+$r_pph21;
        @endphp
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right">PPh.21</td>
          <td></td>
          <td align="right">{{ number_format($r_pph21, 0, ".", ".")}}</td>
          <td align="right">
            @php
            $total_saldo = $total_saldo-$r_pph21;
            @endphp
            {{ number_format($total_saldo, 0, ".", ".")}}
          </td>
          <td></td>
        </tr>
        @endif
        @else
        @endif
        <!-- END IF KODE PPH21 -->
        @else

        @endif
        <!-- END IF KODE PAJAK -->
        @php
        $tanggal_temp=$nota->tanggal;
        @endphp
        @endforeach
        @endforeach
        @endforeach
        <tr height="20px">
          <td colspan="10"></td>
        </tr>
        <tr style="background-color:#ff9898; font-weight:bold;">
          <td colspan="6" align="center">TOTAL PENGELUARAN</td>
          <td align="right">{{ number_format($total_dana, 0, ".", ".")}}</td>
          <td align="right">{{ number_format($total_debet, 0, ".", ".")}}</td>
          <td align="right">{{ number_format($total_saldo, 0, ".", ".")}}</td>
          <td></td>
        </tr>
        <tr height="20px">
          <td colspan="10"></td>
        </tr>
    </tbody>
  </table>

  <div class="row">
    <div class="col-9"></div>
    <div class="col-3">
      @foreach($profil as $prof)
      @if($prof->profil_key==="Desa")
      {{$prof->profil_value}},
      @endif
      @endforeach
      {{ tanggal_local($tgl_dana->tanggal) }}
    </div>
  </div>
  <div class="row">
    <div class="col-3">Menyetujui :</div>
    <div class="col-9"></div>
  </div>
  <div class="row">
    <div class="col-3">Kepala SMK Pelita</div>
    <div class="col-6"></div>
    <div class="col-3">Bendahara / P. Jawab Kegiatan</div>
  </div>
  <br><br><br>
  <div class="row">
    <div class="col-3">
      @foreach($profil as $prof)
      @if($prof->profil_key==="Kepala Sekolah")
      {{$prof->profil_value}}
      @endif
      @endforeach
    </div>
    <div class="col-6"></div>
    <div class="col-3">
      @foreach($profil as $prof)
      @if($prof->profil_key==="Bendahara BOS")
      {{$prof->profil_value}}
      @endif
      @endforeach
    </div>
  </div>

  </body>

</html>