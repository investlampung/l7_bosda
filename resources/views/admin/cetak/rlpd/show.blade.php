<html>

</html>
<!DOCTYPE html>
<html>

<head>
  <title>Rekapitulasi Laporan Penggunaan Dana</title>
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
  <!-- <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet"> -->
  <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

  <style>
    @media print {
      @page {
        size: auto !important
      }
    }

    .samping {
      padding: 15px 15px;
    }

    body {
      font-family: 'Source Sans Pro', sans-serif;
    }

    .tabb, th,tr,td{
      padding: 5px 5px;
    }
  </style>
</head>

<body>
  <!-- onload="window.print()" -->
  <div class="samping">
    <center>
      <h4 class="box-title">REKAPITULASI LAPORAN PENGGUNAAN DANA<br>BANTUAN OPERASIONAL SEKOLAH DAERAH<br>{{Cookie::get('tahap')}} - Tahun {{Cookie::get('tahun')}}</h4>
    </center>
    <br>
    <div class="row">
      <table width="100%">
        <tr>
          <td>Nama Sekolah</td>
          <td>: {{$nama->profil_value}}</td>
          <td></td>
          <td>Periode</td>
          <td>: {{Cookie::get('tahap')}}</td>
        </tr>
        <tr>
          <td>Kabupaten</td>
          <td>: {{$kab->profil_value}}</td>
          <td></td>
          <td>Bulan</td>
          <td>: {{ $bulan->meta_value }}</td>
        </tr>
        <tr>
          <td>Jenis Kegiatan</td>
          <td>: {{$jenis->meta_value}}</td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>
    </div>
    <br>

    <table class="tabb" width="100%" border="1">
      <thead>
        <tr>
          <th rowspan="2" width="20px" align="center">No</th>
          <th rowspan="2" align="center" width="100px">Tanggal</th>
          <th colspan="2" align="center">Transaksi</th>
          <th rowspan="2" align="center">Kode Rekening</th>
          <th rowspan="2" align="center" width="300px">Jenis Kegiatan</th>
          <th rowspan="2" align="center">Nama Barang</th>
          <th rowspan="2" align="center">Volume</th>
          <th rowspan="2" align="center">Satuan</th>
          <th colspan="2" align="center">Harga</th>
        </tr>
        <tr>
          <th>Bukti</th>
          <th width="100px">Tanggal</th>
          <th>Satuan</th>
          <th>Jumlah</th>
        </tr>
      </thead>
      <tbody>
        @php
        $totalll = 0;
        @endphp

        @foreach($komponen as $item)
        @foreach($item->sub_komponen as $subkom)
        @foreach($subkom->nota->sortBy('tanggal') as $nota)
        @php
        $total_pajak = 0;
        $jum_harga = 0;
        $total = 0;
        $j=0;
        @endphp

        <!-- NOTA BARANG -->
        @foreach($nota->nota_barang as $hem)
        @php
        $jum_harga = $jum_harga + ($hem->barang->harga*$hem->qty);
        @endphp

        @if($j==0)
        <tr>
          <td align="center">{{$no}}</td>
          <td align="center">
            @if(empty($hem->nota->tanggal_bku))
            @else
            {{ $hem->nota->tanggal_bku->format('d-m-Y') }}
            @endif
          </td>
          <td align="center">{{$no}}</td>
          <td align="center">{{ $hem->nota->tanggal->format('d-m-Y') }}</td>
          <td align="center">{{$hem->nota->kode_rek}}</td>
          <td>
            @if($nota->uraian=="")
            Pelunasan {{$nota->sub_komponen->sub_komponen}}
            @else
            Pelunasan {{$nota->uraian}}
            @endif
          </td>
          <td>{{$hem->barang->barang}}</td>
          <td align="center">{{$hem->qty}}</td>
          <td align="center">{{$hem->barang->satuan}}</td>
          <td align="right">{{$hem->barang->harga}}</td>
          <td align="right">{{ number_format($hem->barang->harga*$hem->qty, 0, ".", ".")}}</td>
        </tr>
        @else
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td>{{$hem->barang->barang}}</td>
          <td align="center">{{$hem->qty}}</td>
          <td align="center">{{$hem->barang->satuan}}</td>
          <td align="right">{{$hem->barang->harga}}</td>
          <td align="right">{{ number_format($hem->barang->harga*$hem->qty, 0, ".", ".")}}</td>
        </tr>
        @endif
        @php $j++ @endphp
        @endforeach

        <tr height="20px">
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right">{{ number_format($jum_harga, 0, ".", ".")}}</td>
        </tr>

        @php $no++ @endphp
        <!-- IF KODE PAJAK -->
        @if($nota->kode_pajak=="T")
        <!-- IF KODE PPH 22-->
        @if($nota->kode_pph=="22")
        @if($jum_harga>=1000000)
        @php
        $dasar_pajak = (100/110)*$jum_harga;
        $ppn = (10/100)*$dasar_pajak;
        $r_ppn = round($ppn, -2);
        $pph22 = (1.5/100)*$dasar_pajak;
        $r_pph22 = round($pph22, -2);
        $total_pajak = $r_ppn+$r_pph22;
        $total = $jum_harga+$total_pajak;
        $totalll = $totalll+$total;
        @endphp
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right">Pembayaran PPn</td>
          <td></td>
          <td></td>
          <td align="right">{{ number_format($r_ppn, 0, ".", ".")}}</td>
          <td align="right">{{ number_format($r_ppn, 0, ".", ".")}}</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right">Pembayaran PPh.22</td>
          <td></td>
          <td></td>
          <td align="right">{{ number_format($r_pph22, 0, ".", ".")}}</td>
          <td align="right">{{ number_format($r_pph22, 0, ".", ".")}}</td>
        </tr>
        @endif
        <!-- END KODE PPH 22 -->
        <!-- IF KODE PPH 21 -->
        @elseif($nota->kode_pph=="21")
        @if($jum_harga>=1000000)
        @php
        $dasar_pajak = (100/110)*$jum_harga;
        $pph21 = (5/100)*$dasar_pajak;
        $r_pph21 = round($pph21, -2);
        $total_pajak = $r_pph21;
        $total = $jum_harga+$total_pajak;
        $totalll = $totalll+$total;
        @endphp
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td align="right">Pembayaran PPh.21</td>
          <td></td>
          <td></td>
          <td align="right">{{ number_format($r_pph21, 0, ".", ".")}}</td>
          <td align="right">{{ number_format($r_pph21, 0, ".", ".")}}</td>
        </tr>
        @endif
        @else
        @endif
        <!-- END IF KODE PPH21 -->
        @else

        @php
        $total = $jum_harga;
        $totalll = $totalll+$total;
        @endphp
        @endif
        <!-- END IF KODE PAJAK -->

        <tr style="background-color:#ff9898; font-weight:bold;">
          <td align="center" colspan="10">Jumlah</td>
          <td align="right">{{ number_format($total, 0, ".", ".")}}</td>
        </tr>
        @endforeach
        @endforeach
        @endforeach
        <tr height="20px">
          <td align="center" colspan="10"></td>
          <td></td>
        </tr>
        <tr style="background-color:#71bd68; font-weight:bold;">
          <td align="center" colspan="10">Total Pengeluaran</td>
          <td align="right">{{ number_format($totalll, 0, ".", ".")}}</td>
        </tr>
        <tr style="background-color:#ead35e; font-weight:bold;">
          <td align="center" colspan="10">Total Penerimaan</td>
          <td align="right">{{ number_format($total_dana, 0, ".", ".")}}</td>
        </tr>
        <tr style="background-color:#658dd8; font-weight:bold;">
          <td align="center" colspan="10">Saldo Kas</td>
          <td align="right">{{ number_format($saldo, 0, ".", ".")}}</td>
        </tr>
        <tr height="20px">
          <td colspan="10"></td>
          <td></td>
        </tr>
      </tbody>
    </table>

    <div class="row">
      <br>
      <table width="100%">
        <tr>
          <td width="70%"></td>
          <td align="right">
            @foreach($profil as $prof)
            @if($prof->profil_key==="Desa")
            {{$prof->profil_value}},
            @endif
            @endforeach
            ......................... {{Cookie::get('tahun')}}
          </td>
        </tr>
      </table>
    </div>
    <br>
    <div class="row">
      <table width="100%">
        <tr>
          <td align="center">
            Mengetahui / Menyutujui<br>Komite Sekolah
            <br><br><br><br>
            @foreach($profil as $prof)
            @if($prof->profil_key==="Komite Sekolah")
            {{$prof->profil_value}}
            @endif
            @endforeach
          </td>
          <td align="center">
            Bendahara
            <br><br><br><br>
            @foreach($profil as $prof)
            @if($prof->profil_key==="Bendahara Sekolah")
            <br>
            {{$prof->profil_value}}
            @endif
            @endforeach
          </td>
          <td align="center">
            Kepala SMK Pelita
            <br><br><br><br>
            @foreach($profil as $prof)
            @if($prof->profil_key==="Kepala Sekolah")
            <br>
            {{$prof->profil_value}}
            @endif
            @endforeach
          </td>
        </tr>
      </table>
    </div>
  </div>

</body>

</html>