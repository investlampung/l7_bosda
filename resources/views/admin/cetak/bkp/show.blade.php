<html>

</html>
<!DOCTYPE html>
<html>

<head>
  <title>BUKTI KAS PENGELUARAN</title>
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- <link href="{{ asset('itlabil/admin/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"> -->

  <style>
    @media print {
      @page {
        size: auto !important
      }
    }

    .samping {
      padding: 15px 15px;
    }

    body {
      font-family: 'Source Sans Pro', sans-serif;
      margin: 15px 15px;
    }
  </style>
</head>

<body>
  <!-- onload="window.print()" -->
  <div class="samping">
    @foreach($komponen as $item)
    @foreach($item->sub_komponen as $subkom)
    @foreach($subkom->nota->sortBy('tanggal') as $nota)
    <center>
      <h4>BUKTI KAS PENGELUARAN (BKP)</h4>
    </center>
    <br>
    @php
    $total_pajak = 0;
    $jum_harga = 0;
    $r_pph21=0;
    $r_pph22=0;
    $r_ppn=0;
    @endphp

    @foreach($nota->nota_barang as $hem)
    @php
    $jum_harga = $jum_harga + ($hem->barang->harga*$hem->qty);
    @endphp
    @endforeach

    <!-- IF KODE PAJAK -->
    @if($nota->kode_pajak=="T")
    <!-- IF KODE PPH 22-->
    @if($nota->kode_pph=="22")
    @if($jum_harga>=1000000)
    @php
    $dasar_pajak = (100/110)*$jum_harga;
    $ppn = (10/100)*$dasar_pajak;
    $r_ppn = round($ppn, -2);
    $pph22 = (1.5/100)*$dasar_pajak;
    $r_pph22 = round($pph22, -2);
    $total_pajak = $r_ppn+$r_pph22;
    @endphp
    @endif
    <!-- END KODE PPH 22 -->
    <!-- IF KODE PPH 21 -->
    @elseif($nota->kode_pph=="21")
    @if($jum_harga>=1000000)
    @php
    $dasar_pajak = (100/110)*$jum_harga;
    $pph21 = (5/100)*$dasar_pajak;
    $r_pph21 = round($pph21, -2);
    $total_pajak = $r_pph21;
    @endphp
    @endif
    @else
    @endif
    <!-- END IF KODE PPH21 -->
    @else
    @endif
    <!-- END IF KODE PAJAK -->
    <div class="row" style="margin-bottom: 20px;">
      <table width="100%" style="border:0; margin:5px 5px;">
        <tr>
          <td width="20%">
            Telah terima dari
          </td>
          <td width="50%">
            : Bendahara BOSDA SMK Pelita Gedongtataan
          </td>
          <td width="30%" rowspan="3">
            <table border="1">
              <tr>
                <td style="padding: 3px;">
                  @php
                  $jml_no = strlen($no);
                  @endphp
                  @if($jml_no===1)
                  Nomor : 00{{$no}}
                  @elseif($jml_no===2)
                  Nomor : 0{{$no}}
                  @else
                  {{$no}}
                  @endif
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          @php
          $text_uang = terbilang($jum_harga);
          @endphp
          <td>Banyaknya Uang</td>
          <td>: <i>{{ucwords($text_uang)}}</i></td>
        </tr>
        <tr>
          <td>Untuk Pembayaran</td>
          <td>
            @if($nota->uraian=="")
            : {{$nota->sub_komponen->sub_komponen}}
            @else
            : {{$nota->uraian}}
            @endif
          </td>
        </tr>
      </table>
    </div>

    <div class="row" style="margin-bottom: 20px;">
      <div class="col-md-1"></div>
      <div class="col-md-11">
        <table border="1">
          <tr>
            <td style="padding: 3px;" width="200px">Rp. {{ number_format($jum_harga, 0, ".", ".")}}</td>
          </tr>
        </table>
      </div>
    </div>

    <div class="row" style="margin-bottom: 20px;">
      <table width="100%" style="border:0; margin:5px 5px;">
        <tr>
          <td>
            Mengetahui / Menyetujui,<br>
            Pengguna Anggaran<br><br><br><br>
            <u>
              <b>
                @foreach($profil as $prof)
                @if($prof->profil_key==="Kepala Sekolah")
                {{$prof->profil_value}}
                @endif
                @endforeach
              </b>
            </u>
          </td>
          <td>
            <br>Ketua Pelaksana
            <br><br><br><br>
            <u><b>
                @foreach($profil as $prof)
                @if($prof->profil_key==="Ketua Pelaksana")
                {{$prof->profil_value}}
                @endif
                @endforeach
              </b></u>

          </td>
          <td>
            <br>Bendahara
            <br><br><br><br>
            <u><b>
                @foreach($profil as $prof)
                @if($prof->profil_key==="Bendahara BOS")
                {{$prof->profil_value}}
                @endif
                @endforeach
              </b></u>
          </td>
          <td>
            @foreach($profil as $prof)
            @if($prof->profil_key==="Desa")
            {{$prof->profil_value}},
            @endif
            @endforeach
            {{ tanggal_local($tgl_dana->tanggal) }}<br>
            Yang Menerima
            <br><br><br><br>
            <u><b>{{$hem->barang->toko->pemilik}}</b></u>
          </td>
        </tr>
      </table>
    </div>
  </div>

  <table border="1">
    <thead>
      <tr>
        <th rowspan="7" width="150px" style="padding:5px 5px; text-align:left;vertical-align:top;">Barang Tersebut Diterima Dengan Baik Oleh</th>
        <th colspan="2" rowspan="2" width="300px" style="padding:5px 5px;text-align:center;vertical-align:middle;">Pajak yang Dipungut</th>
        <th colspan="3" style="padding:5px 5px;text-align:center;vertical-align:center;">Pembebanan Terhadap Belanja</th>
      </tr>
      <tr>
        <th width="100px" style="padding:5px 5px;text-align:center;vertical-align:center;">No Bukti</th>
        <th style="padding:5px 5px;text-align:center;vertical-align:center;">Uraian</th>
        <th style="padding:5px 5px;text-align:center;vertical-align:center;">Jumlah</th>
      </tr>
      <tr>
        <td width="80px" style="padding:5px 5px;">PPn</td>
        <td width="100px" style="padding:5px 5px;">
          @if($r_ppn==0)
          @else
          Rp. {{ number_format($r_ppn, 0, ".", ".")}}
          @endif
        </td>
        <td align="center" style="padding:5px 5px;">
          @php
          $jml_no = strlen($no);
          @endphp
          @if($jml_no===1)
          00{{$no}}
          @elseif($jml_no===2)
          0{{$no}}
          @else
          {{$no}}
          @endif
        </td>
        <td rowspan="3" style="padding:5px 5px;">
          @if($nota->uraian=="")
          {{$nota->sub_komponen->sub_komponen}}
          @else
          {{$nota->uraian}}
          @endif
        </td>
        <td style="padding:5px 5px;">Rp. {{ number_format($jum_harga, 0, ".", ".")}}</td>
      </tr>
      <tr>
        <td style="padding:5px 5px;">PPh.22</td>
        <td style="padding:5px 5px;">
          @if($r_pph22==0)
          @else
          Rp. {{ number_format($r_pph22, 0, ".", ".")}}
          @endif
        </td>
        <td style="padding:5px 5px;"></td>
        <td style="padding:5px 5px;"></td>
      </tr>
      <tr>
        <td style="padding:5px 5px;">PPh.21</td>
        <td style="padding:5px 5px;">
          @if($r_pph21==0)
          @else
          Rp. {{ number_format($r_pph21, 0, ".", ".")}}
          @endif
        </td>
        <td style="padding:5px 5px;"></td>
        <td style="padding:5px 5px;"></td>
      </tr>
      <tr height="20px">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr style="font-weight:bold;">
        <td style="padding:5px 5px;">Jumlah</td>
        <td style="padding:5px 5px;">
          @if($r_ppn==0)
          @else
          Rp. {{ number_format($total_pajak, 0, ".", ".")}}
          @endif
        </td>
        <td colspan="2" align="center" style="padding:5px 5px;">Jumlah</td>
        <td width="120px" style="padding:5px 5px;">Rp. {{ number_format($jum_harga, 0, ".", ".")}}</td>
      </tr>
    </thead>
    <tbody>

    </tbody>
  </table>
  @php $no++; @endphp

  <div style='page-break-after:always'></div>
  @endforeach
  @endforeach
  @endforeach
  </div>

</body>

</html>