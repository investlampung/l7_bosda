@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Bukti Kas Pengeluaran
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <a class="btn btn-primary" target="_blank" href="{{ route('bkp.show',1) }}"><i class="fa fa-print"></i> Cetak BKP</a>
      <br><br>

      @foreach($komponen as $item)
      @foreach($item->sub_komponen as $subkom)
      @foreach($subkom->nota->sortBy('tanggal') as $nota)
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">BUKTI KAS PENGELUARAN (BKP)</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          @php
          $total_pajak = 0;
          $jum_harga = 0;
          $r_pph21=0;
          $r_pph22=0;
          $r_ppn=0;
          @endphp

          @foreach($nota->nota_barang as $hem)
          @php
          $jum_harga = $jum_harga + ($hem->barang->harga*$hem->qty);
          @endphp
          @endforeach

          <!-- IF KODE PAJAK -->
          @if($nota->kode_pajak=="T")
          <!-- IF KODE PPH 22-->
          @if($nota->kode_pph=="22")
          @if($jum_harga>=1000000)
          @php
          $dasar_pajak = (100/110)*$jum_harga;
          $ppn = (10/100)*$dasar_pajak;
          $r_ppn = round($ppn, -2);
          $pph22 = (1.5/100)*$dasar_pajak;
          $r_pph22 = round($pph22, -2);
          $total_pajak = $r_ppn+$r_pph22;
          @endphp
          @endif
          <!-- END KODE PPH 22 -->
          <!-- IF KODE PPH 21 -->
          @elseif($nota->kode_pph=="21")
          @if($jum_harga>=1000000)
          @php
          $dasar_pajak = (100/110)*$jum_harga;
          $pph21 = (5/100)*$dasar_pajak;
          $r_pph21 = round($pph21, -2);
          $total_pajak = $r_pph21;
          @endphp
          @endif
          @else
          @endif
          <!-- END IF KODE PPH21 -->
          @else
          @endif
          <!-- END IF KODE PAJAK -->

          <div class="col-md-12" style="margin-bottom: 20px;">
            <div class="col-md-2">
              Telah terima dari<br>
              Banyaknya Uang<br>
              Untuk Pembayaran
            </div>
            <div class="col-md-7">
              @php
              $text_uang = terbilang($jum_harga);
              @endphp
              <i>
                Bendahara BOS SMK Pelita Gedongtataan<br>
                {{ucwords($text_uang)}}<br>
                @if($nota->uraian=="")
                {{$nota->sub_komponen->sub_komponen}}
                @else
                {{$nota->uraian}}
                @endif
              </i>
            </div>
            <div class="col-md-3">
              <table border="1">
                <tr>
                  <td style="padding: 3px;">
                    @php
                    $jml_no = strlen($no);
                    @endphp
                    @if($jml_no===1)
                    Nomor : 00{{$no}}
                    @elseif($jml_no===2)
                    Nomor : 0{{$no}}
                    @else
                    {{$no}}
                    @endif
                  </td>
                </tr>
              </table>
            </div>
          </div>

          <div class="col-md-12" style="margin-bottom: 20px;">
            <div class="col-md-1"></div>
            <div class="col-md-11">
              <table border="1">
                <tr>
                  <td style="padding: 3px;" width="200px">Rp. {{ number_format($jum_harga, 0, ".", ".")}}</td>
                </tr>
              </table>
            </div>
          </div>

          <div class="col-md-12" style="margin-bottom: 20px;">
            <div class="col-md-3" align="center" style="text-align:center;">
              Mengetahui / Menyetujui,<br>
              Pengguna Anggaran<br><br><br>
            </div>
            <div class="col-md-3" align="center" style="text-align:center;">
              <br>Ketua Pelaksana
            </div>
            <div class="col-md-3" align="center" style="text-align:center;">
              <br>Bendahara
            </div>
            <div class="col-md-3" align="center" style="text-align:center;">
              @foreach($profil as $prof)
              @if($prof->profil_key==="Desa")
              {{$prof->profil_value}},
              @endif
              @endforeach
              {{ tanggal_local($tgl_dana->tanggal) }}<br>
              Yang Menerima
            </div>
          </div>

          <div class="col-md-12" style="margin-bottom: 20px;">
            <div class="col-md-3" align="center" style="text-align:center;">
              <u><b>
                  @foreach($profil as $prof)
                  @if($prof->profil_key==="Kepala Sekolah")
                  {{$prof->profil_value}}
                  @endif
                  @endforeach
                </b></u>
            </div>
            <div class="col-md-3" align="center" style="text-align:center;">
              <u><b>
                  @foreach($profil as $prof)
                  @if($prof->profil_key==="Ketua Pelaksana")
                  {{$prof->profil_value}}
                  @endif
                  @endforeach
                </b></u>
            </div>
            <div class="col-md-3" align="center" style="text-align:center;">
              <u><b>
                  @foreach($profil as $prof)
                  @if($prof->profil_key==="Bendahara BOS")
                  {{$prof->profil_value}}
                  @endif
                  @endforeach
                </b></u>
            </div>
            <div class="col-md-3" align="center" style="text-align:center;">
              <u><b>{{$hem->barang->toko->pemilik}}</b></u>
            </div>
          </div>

          <table class="table table-bordered">
            <thead>
              <tr>
                <th rowspan="7" width="150px" style="text-align:left;vertical-align:top;">Barang Tersebut Diterima Dengan Baik Oleh</th>
                <th colspan="2" rowspan="2" width="300px" style="text-align:center;vertical-align:middle;">Pajak yang Dipungut</th>
                <th colspan="3" style="text-align:center;vertical-align:center;">Pembebanan Terhadap Belanja</th>
              </tr>
              <tr>
                <th width="100px" style="text-align:center;vertical-align:center;">No Bukti</th>
                <th style="text-align:center;vertical-align:center;">Uraian</th>
                <th style="text-align:center;vertical-align:center;">Jumlah</th>
              </tr>
              <tr>
                <td width="80px">PPn</td>
                <td>
                  @if($r_ppn==0)
                  @else
                  Rp. {{ number_format($r_ppn, 0, ".", ".")}}
                  @endif
                </td>
                <td align="center">
                  @php
                  $jml_no = strlen($no);
                  @endphp
                  @if($jml_no===1)
                  00{{$no}}
                  @elseif($jml_no===2)
                  0{{$no}}
                  @else
                  {{$no}}
                  @endif
                </td>
                <td rowspan="3">
                  @if($nota->uraian=="")
                  {{$nota->sub_komponen->sub_komponen}}
                  @else
                  {{$nota->uraian}}
                  @endif
                </td>
                <td>Rp. {{ number_format($jum_harga, 0, ".", ".")}}</td>
              </tr>
              <tr>
                <td>PPh.22</td>
                <td>
                  @if($r_pph22==0)
                  @else
                  Rp. {{ number_format($r_pph22, 0, ".", ".")}}
                  @endif
                </td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td>PPh.21</td>
                <td>
                  @if($r_pph21==0)
                  @else
                  Rp. {{ number_format($r_pph21, 0, ".", ".")}}
                  @endif
                </td>
                <td></td>
                <td></td>
              </tr>
              <tr height="20px">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr style="font-weight:bold;">
                <td>Jumlah</td>
                <td>
                  @if($r_ppn==0)
                  @else
                  Rp. {{ number_format($total_pajak, 0, ".", ".")}}
                  @endif
                </td>
                <td colspan="2" align="center">Jumlah</td>
                <td>Rp. {{ number_format($jum_harga, 0, ".", ".")}}</td>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
      @php $no++; @endphp
      @endforeach
      @endforeach
      @endforeach
    </div>
  </div>
</div>
@endsection