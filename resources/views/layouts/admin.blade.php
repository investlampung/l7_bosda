<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset ('itlabil/image/default/logo.png') }}">
    <title>Admin SEMKAPITA</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- CSS -->

    <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/morris.js/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/jvectormap/jquery-jvectormap.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="{{ asset('itlabil/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link href="{{ asset('itlabil/admin/toast/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/dist/css/editsendiri.css') }}" rel="stylesheet">
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <!-- HEADER -->
        <header class="main-header">
            <a href="{{ asset('admin/beranda') }}" class="logo">
                <span class="logo-mini">BDA</span>
                <span class="logo-lg"><b>BOSDA</b></span>
            </a>
            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ asset('itlabil/admin/dist/img/avatar5.png') }}" class="user-image" alt="User Image">
                                <span class="hidden-xs">{{Auth::user()->name}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="{{ asset('itlabil/admin/dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
                                    <p>{{Auth::user()->name}}</p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div align="center">
                                        <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <!-- Control Sidebar Toggle Button -->
                        <!-- <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li> -->
                    </ul>
                </div>
            </nav>
        </header>

        <!-- MENU SIDE BAR -->
        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu" data-widget="tree">
                    <li style="background-color:#206288;"><a href="#" data-toggle="modal" data-target="#modal-tahun"><span>Tahun Aktif : {{ Cookie::get('tahun') }}</span></a></li>
                    <li style="background-color:#206288;"><a href="#" data-toggle="modal" data-target="#modal-tahap"><span>Tahap Aktif : {{ Cookie::get('tahap') }}</span></a></li>
                    <li><a href="{{ asset('admin/beranda') }}"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
                    <li><a href="{{ asset('admin/profil') }}"><i class="fa fa-bank"></i> <span>Data Sekolah</span></a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-money"></i>
                            <span>Dana & Saldo</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ asset('admin/dana') }}"><i class="fa fa-circle-o"></i> Dana</a></li>
                            <li><a href="{{ asset('admin/saldo') }}"><i class="fa fa-circle-o"></i> Saldo</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ asset('admin/toko') }}"><i class="fa fa-bank"></i> Toko</a></li>
                    <li><a href="{{ asset('admin/barang') }}"><i class="fa fa-dropbox"></i> Barang</a></li>
                    <li><a href="{{ asset('admin/komponen') }}"><i class="fa fa-book"></i> Alokasi Pengelolaan Dana</a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-print"></i>
                            <span>Cetak</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ asset('admin/cetak/notas') }}"><i class="fa fa-circle-o"></i> Nota</a></li>
                            <li><a href="{{ asset('admin/cetak/apd') }}"><i class="fa fa-circle-o"></i> Alokasi Pengelolaan Dana</a></li>
                            <li><a href="{{ asset('admin/cetak/bku') }}"><i class="fa fa-circle-o"></i> Buku Kas Umum</a></li>
                            <li><a href="{{ asset('admin/cetak/bpp') }}"><i class="fa fa-circle-o"></i> Buku Pembantu Pajak</a></li>
                            <li><a href="{{ asset('admin/cetak/rlpd') }}"><i class="fa fa-circle-o"></i> RLP Dana</a></li>
                            <li><a href="{{ asset('admin/cetak/bkp') }}"><i class="fa fa-circle-o"></i> Bukti Kas Pengeluaran</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ asset('admin/history') }}"><i class="fa fa-list"></i> <span>History</span></a></li>
                    <li><a href="{{ asset('admin/pengaturan') }}"><i class="fa fa-cog"></i> <span>Pengaturan</span></a></li>
                </ul>
            </section>
        </aside>
        <!-- TAHUN -->
        <div class="modal fade in" id="modal-tahun">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Tahun Aktif</h4>
                    </div>
                    <form action="{{ route('rubah.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="box-body">
                            <div class="form-group">
                                <label for="tahun" class="col-sm-2 control-label">Tahun</label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="tahun">
                                        @foreach(App\Tahun::select('tahun')->get()->all() as $all_tahun)
                                        <option value="{{$all_tahun->tahun}}">{{$all_tahun->tahun}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="tipe" value="tahun">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <!-- TAHAP -->

        <div class="modal fade in" id="modal-tahap">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Tahap Aktif</h4>
                    </div>
                    <form action="{{ route('rubah.store') }}" class="form-horizontal" method="POST">
                        @csrf

                        <div class="box-body">
                            <div class="form-group">
                                <label for="tahun" class="col-sm-2 control-label">Tahun</label>

                                <div class="col-sm-10">
                                    <select class="form-control" name="tahap">
                                        @foreach(App\Tahap::select('tahap')->where('tahun_id',Cookie::get('id_tahun'))->get()->all() as $all_tahap)
                                        <option value="{{$all_tahap->tahap}}">{{$all_tahap->tahap}}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="tipe" value="tahap">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <!-- CONTENT -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content')
            </section>
        </div>

        <!-- FOOTER -->
        <footer class="main-footer">
            <strong>Copyright &copy; 2019 <a href="#">SMK Pelita Pesawaran</a>
        </footer>

        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- JS -->
    <script src="{{ asset('itlabil/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="{{ asset('itlabil/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/morris.js/morris.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('itlabil/admin/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/dist/js/pages/dashboard.js') }}"></script>
    <script src="{{ asset('itlabil/admin/dist/js/demo.js') }}"></script>


    <!-- DataTables -->
    <script src="{{ asset('itlabil/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('itlabil/admin/toast/toastr.min.js') }}"></script>

    <script>
        @if(Session::has('message'))
        var type = "{{Session::get('alert-type','info')}}"

        switch (type) {
            case 'info':
                toastr.info("{{ Session::get('message') }}");
                break;
            case 'success':
                toastr.success("{{ Session::get('message') }}");
                break;
            case 'warning':
                toastr.warning("{{ Session::get('message') }}");
                break;
            case 'error':
                toastr.error("{{ Session::get('message') }}");
                break;
        }
        @endif
    </script>

    <script>
        $(function() {
            $('#example1').DataTable()
            $('#example2').DataTable()
            $('#example3').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
</body>

</html>